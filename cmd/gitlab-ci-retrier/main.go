package main

import (
	"log"
	"os"
	"time"

	"github.com/xanzy/go-gitlab"
)

func listAllPipelineJobs(gl *gitlab.Client, pid interface{}, pipelineID int, opts *gitlab.ListJobsOptions, options ...gitlab.RequestOptionFunc) ([]*gitlab.Job, error) {
	if opts == nil {
		opts = &gitlab.ListJobsOptions{}
	}
	listJobOptions := *opts
	listJobOptions.PerPage = 100
	listJobOptions.Page = 1

	var jobs []*gitlab.Job
	for {
		newJobs, _, err := gl.Jobs.ListPipelineJobs(pid, pipelineID, &listJobOptions, options...)
		if err != nil {
			return nil, err
		}

		if len(newJobs) == 0 {
			return jobs, nil
		}

		jobs = append(jobs, newJobs...)
		listJobOptions.Page += 1
	}
}

func pipelineStuck(gl *gitlab.Client, pid interface{}, pipelineID int) (bool, error) {
	jobs, err := listAllPipelineJobs(gl, pid, pipelineID, nil)
	if err != nil {
		return false, err
	}

	for _, job := range jobs {
		if job.Status != "created" {
			return false, nil
		}
	}

	return true, nil
}

func main() {
	gl, err := gitlab.NewClient(os.Getenv("GITLAB_CI_RETRIER_TOKEN"))
	if err != nil {
		panic(err)
	}

	pid := os.Getenv("GITLAB_CI_RETRIER_PID")

	updatedAfter := time.Now().Add(-10 * time.Minute)

	pipelines, _, err := gl.Pipelines.ListProjectPipelines(pid, &gitlab.ListProjectPipelinesOptions{
		Status:       gitlab.BuildState(gitlab.Failed),
		UpdatedAfter: &updatedAfter,
		ListOptions: gitlab.ListOptions{
			PerPage: 100, // yolo
		},
	})
	if err != nil {
		panic(err)
	}

	for _, pipeline := range pipelines {
		stuck, err := pipelineStuck(gl, pid, pipeline.ID)
		if err != nil {
			log.Print(err)
			continue
		}
		if stuck {
			_, _, err := gl.Pipelines.RetryPipelineBuild(pid, pipeline.ID)
			if err != nil {
				log.Printf("cannot restart pipeline %d", pipeline.ID)
				log.Print(err)
				continue
			}
			log.Printf("restarted pipeline %d", pipeline.ID)
		}
	}
}
